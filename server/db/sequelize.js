const path = require('path')
const {Sequelize} = require('sequelize');
const log4js = require('log4js');
const logger_task = log4js.getLogger('query');

let config = require(path.join(__dirname, '../config/config.js'));

// region Overwrite config from env
const env = process.env.NODE_ENV;
if(!env){ // default local env
    config = config['local'];
}else {
    config = config[env];
}
// endregion


const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: config.dialect,
    logging: customLogger,
    pool: {
        max: config.pool.max,
        min: config.pool.min,
        acquire: config.pool.acquire,
        idle: config.pool.idle,
    },
});
// const db = {};
// db.Sequelize = Sequelize;
// db.sequelize = sequelize;
// db.images = require('../models/Product')(sequelize, Sequelize);
// db.images = require('../src/models/Product')(sequelize, Sequelize);

// module.exports = db;

function customLogger ( queryString, queryObject ) {
    if(env != 'production'){
        logger_task.info('SQL query:\r\n', queryString )      // outputs a string
        logger_task.info('Bind:\r\n', queryObject.bind ) // outputs an array
    }
}

// test if dev
if (env != 'production') {

    sequelize.authenticate().then(() => {
        console.log('Kết nối thành công với database');
    }).catch((error) => {
        console.error('Không thể kết nối với database', error);
    });
}

module.exports = sequelize
