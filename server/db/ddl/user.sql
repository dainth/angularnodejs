CREATE SEQUENCE user_id_seq;
CREATE TABLE public.user
(
    id bigint NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    code bigint NOT NULL,
    username character varying(1536) NOT NULL COLLATE pg_catalog."default",
    password character varying(1536) NOT NULL COLLATE pg_catalog."default",
    email character varying(1536) NOT NULL COLLATE pg_catalog."default",
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    CONSTRAINT user_pkey PRIMARY KEY (id)
)