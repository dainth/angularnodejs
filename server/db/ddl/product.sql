-- Table: public.memo_iphone
-- DROP TABLE public.memo_iphone;
CREATE SEQUENCE product_id_seq1;
CREATE TABLE public.product1
(
    id bigint NOT NULL DEFAULT nextval('product_id_seq1'::regclass),
    code bigint NOT NULL,
    number bigint NOT NULL,
    name character varying(1536) NOT NULL COLLATE pg_catalog."default",
    image character varying(1536) NOT NULL COLLATE pg_catalog."default",
    status character varying(1536) NOT NULL COLLATE pg_catalog."default",
    date_manufacture timestamp without time zone,
    date_expiration timestamp without time zone,
    insert_at timestamp without time zone,
    update_at timestamp without time zone,
    CONSTRAINT product_pkey PRIMARY KEY (id)
)