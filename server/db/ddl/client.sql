-- Table: public.memo_iphone
-- DROP TABLE public.memo_iphone;
CREATE SEQUENCE client_id_seq;
CREATE TABLE public.client
(
    id bigint NOT NULL DEFAULT nextval('client_id_seq'::regclass),
    code bigint NOT NULL,
    name character varying(1536) NOT NULL COLLATE pg_catalog."default",
    address character varying(1536) NOT NULL COLLATE pg_catalog."default",
    telephone CHAR (11) NOT NULL DEFAULT 0,
    status character varying(255) NOT NULL COLLATE pg_catalog."default",,
    insert_at timestamp without time zone,
    update_at timestamp without time zone,
    birthDate timestamp without time zone,
    CONSTRAINT client_pkey PRIMARY KEY (id)
)