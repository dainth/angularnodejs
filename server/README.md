# AngularNodeCrud

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Backend Node server

Run `npm run dev` at server folder for start node backend server at 3001 port.

## Frontend Angular server

Run `npm run start` at frontend folder for start Angular server with proxy pointing to backend server