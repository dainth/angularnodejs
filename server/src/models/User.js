const sequelize = require('../../db/sequelize');
const {
    Model, DataTypes
} = require('sequelize');
module.exports = () => {
    class User extends Model {
        static associate(models) {
            // define association here
        }
    }

    User.init({
        code: DataTypes.BIGINT,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        username: DataTypes.STRING,
        created_at: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        },
    }, {
        sequelize,
        timestamps: false,
        modelName: 'User',
        tableName:'user'
    });
    return User;
};
