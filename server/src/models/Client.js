const sequelize = require('../../db/sequelize');
const {
	Model, DataTypes
} = require('sequelize');
module.exports = () => {
	class Client extends Model {
		static associate(models) {
			// define association here
		}
	}

	Client.init({
		code: DataTypes.BIGINT,
		name: DataTypes.STRING,
		address: DataTypes.STRING,
		telephone: DataTypes.CHAR,
		status: DataTypes.STRING,
		birthdate: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: DataTypes.NOW
		},
	}, {
		sequelize,
		timestamps: false,
		modelName: 'Client',
		tableName:'client'
	});
	return Client;
};
