const sequelize = require('../../db/sequelize');
const {
    Model, DataTypes
} = require('sequelize');
module.exports = () => {
    class Product extends Model {
        static associate(models) {
            // define association here
        }
    }

    Product.init({
        code: DataTypes.BIGINT,
        name: DataTypes.STRING,
        status: DataTypes.STRING,
        number: DataTypes.BIGINT,
        date_manufacture: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        },
        date_expiration: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        }
    }, {
        sequelize,
        timestamps: false,
        modelName: 'Product',
        tableName:'product'
    });
    return Product;
};
