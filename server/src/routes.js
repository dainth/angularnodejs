const express = require('express');
const router = express.Router();

const ProductController = require('./controllers/ProductController');
const ClientController = require('./controllers/ClientController');
const UserController = require('./controllers/UserController');
//Client
router.get('/client', ClientController.index);
router.get('/client/:id', ClientController.show);
router.post('/client', ClientController.store);
router.put('/client/:id', ClientController.update);
router.delete('/client/:id', ClientController.destroy);
//Product
router.get('/product', ProductController.index);
router.get('/product/:id', ProductController.show);
router.post('/product', ProductController.store);
router.post('/product/uploadImage', ProductController.uploadImage);
router.put('/product/:id', ProductController.update);
router.delete('/product/:id', ProductController.destroy);

router.post('/register', UserController.register);
router.post('/login', UserController.login);
module.exports = router;
