const Product = require('../models/Product');
const multer = require('multer');
const config = require('../../config/config.json');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __basedir + '/resources/static/assets/uploads/')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
    }
});

var upload = multer({storage: storage});

module.exports = {
    upload,
    async index(req, res) {
        const {page = 1, limit = 10} = req.query;
        const products = await Product().findAll();
        res.status(200).json(products);
    },

    async show(req, res) {
        const product = await Product().findByPk(req.params.id);
        res.status(200).json(product)
        // res.status(200).json(product);
    },

    async store(req, res) {
        const data = {
            code: req.body.code,
            // image: req.file.image,
            name: req.body.name,
            date_manufacture: req.body.date_manufacture,
            date_expiration: req.body.date_expiration,
            number: req.body.number,
            status: req.body.status
        };
        try {
            const product = await Product().create(data);
            return res.json(product);
        } catch (err) {
            return res.status(414).send(err);
        }
    },

    async update(req, res) {
        try {
            const productId = req.params.id;
            const data = {
                code: req.body.code,
                name: req.body.name,
                date_manufacture: req.body.date_manufacture,
                date_expiration: req.body.date_expiration,
                number: req.body.number,
                status: req.body.status
            };
            const product = await Product().update(data, {
                where: {
                    id: productId
                }
            });
            return res.json(product);
        } catch (err) {
            return res.status(414).send(err);
        }
    },

    async destroy(req, res) {
        await Product().destroy({
            where: {
                id: req.params.id,
            }
        });
        return res.send();
    },
    async uploadImage(req, res) {
        upload.single('image');
        if (!req.file) {
            console.log("No file is available!");
            return res.send({
                success: false
            });

        } else {
            console.log('File is available!');
            return res.send({
                success: true
            })
        }
    },
};
