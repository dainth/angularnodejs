const Client = require('../models/Client');
// const Client = mongoose.model('Client');


module.exports = {
	async index(req, res) {
		const { page = 1, limit = 10 } = req.query;
		//const clients = await Client.paginate({}, { page, limit });
		const clients = await Client().findAll();
		res.status(200).json(clients);
	},

	async show(req, res) {
		const client = await Client().findByPk(req.params.id);
		res.status(200).json(client);
	},

	async store(req, res) {
		const data = {
			code: req.body.code,
			name: req.body.name,
			address: req.body.address,
			telephone: req.body.telephone,
			birthdate: req.body.birthdate,
			status: req.body.status
		}
		try {
			const client = await Client().create(data);
			return res.json(client);
		} catch(err) {
			return res.status(414).send(err);
		}		
	},

	async update(req, res) {
		try {
			const clientId = req.params.id;
			const data = {
				code: req.body.code,
				name: req.body.name,
				address: req.body.address,
				telephone: req.body.telephone,
				birthdate: req.body.birthdate,
				status: req.body.status
			}
			const client = await Client().update(data, {
				where: {
					id: clientId
				}
			});
			return res.json(client);
		} catch (err) {
			return res.status(414).send(err);
		}
	},

	async destroy(req, res) {
		await Client().destroy(req.params.id);
		return res.send();
	}
};
