const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const __ = require('lodash')
const config = require('../../config/config.json');
module.exports = {
    async register(req, res) {
        const hash = bcrypt.hashSync(req.body.password, 10);
        req.body.password = await bcrypt.hash(req.body.password, 12);
        const data = {
            username: req.body.username,
            code: req.body.code,
            email: req.body.email,
            password: req.body.password,
        };
        try {
            const user = await User().create(data);
            return res.json(user);
        } catch (err) {
            return res.status(414).send(err);
        }
    },
    async login(req, res) {
        // const email = req.body.email;
        // const password = req.body.password;
        //
        // // User.verifyCredentials({ username: username, password : password }, (error, response) => {
        //
        // // if(error) return res.;
        //
        //
        // // return res.su;
        //
        //
        // // });
        //
        //
        // User.findOne({
        //     where: {
        //         email: email
        //     }
        // }).then((user) => {
        //
        //     if (user) {
        //
        //         user.comparePassword(password, (error, response) => {
        //             if (error) return res.status(401).json(handleUnAuthorizedError);
        //
        //             if (response) {
        //                 const payload = {
        //                     email: email
        //                 };
        //                 var token = jwt.sign(payload, app.get('superSecret'), {
        //                     expiresIn: '24h' // expires in 24 hours
        //                 });
        //
        //                 // return the information including token as JSON
        //                 res.json({
        //                     success: true,
        //                     message: 'Enjoy your token!',
        //                     token: token
        //                 });
        //             } else {
        //                 return res.status(401).json(handleUnAuthorizedError);
        //             }
        //
        //         });
        //     } else {
        //         res.status(401).json(handleUnAuthorizedError);
        //     }
        //     ;
        //
        // }).catch((error) => res.status(401).json(handleUnAuthorizedError));
    },
    // async handleUnAuthorizedError() {
    //     success: false,
    //         message;
    // :
    //     'UnAuthorized',
    //
    //         token
    // :
    //     null
    // }
}
