const fs = require('fs');

const config = {
    local: {
        username: 'postgres',
        user: 'postgres',
        password: '20111998',
        database: 'nodejs',
        host: 'localhost',
        port: 5432,
        dialect: 'postgres',
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    },
    development: {
        username: 'postgres',
        user: 'postgres',
        password: '20111998',
        database: 'nodejs',
        host: 'localhost',
        port: 5432,
        dialect: 'postgres'
    },
    production: {
        username: process.env.PROD_DB_USERNAME,
        password: process.env.PROD_DB_PASSWORD,
        database: process.env.PROD_DB_NAME,
        host: process.env.PROD_DB_HOSTNAME,
        port: process.env.PROD_DB_PORT,
        dialect: 'postgres',
        /*dialectOptions: {
            bigNumberStrings: true,
            ssl: {
                ca: fs.readFileSync(__dirname + '/mysql-ca-master.crt')
            }
        }*/
    }
};

// region use default config
const env = process.env.NODE_ENV;
// if (!env) {
//     const cf = require('./postgres.json')
//     // migrations
//     config.local.host = cf.host
//     config.local.port = cf.port
//     config.local.database = cf.database;
//     config.local.user = cf.user
//     config.local.username = cf.user
//     config.local.password = cf.password
//
// }

//endregion

module.exports = config;