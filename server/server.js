const express = require('express');
const Sequelize = require('sequelize')
//const cors = require('cors');
const mongoose = require('mongoose');
const requireDir = require('require-dir');
var bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// mongoose.connect('mongodb://localhost:27017/nodeapi', { useNewUrlParser: true });
// your credentialsy
requireDir('./src/models');
app.use('/api', require('./src/routes'));


app.listen(3001);
