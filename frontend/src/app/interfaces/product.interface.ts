export interface Product {
  name: string;
  code: number;
  date_manufacture: Date;
  date_expiration: Date;
  status: string;
  number: string;
}
