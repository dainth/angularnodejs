import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClienteCadComponent } from './components/cliente-cad.component';
import { ClienteListComponent } from './components/cliente-list.component';
import {ProductComponent} from './components/product/product.component';
import {ProductCadComponent} from './components/product/product-cad.component';

const routes: Routes = [
  { path: 'cliente/cad', component: ClienteCadComponent },
  { path: 'cliente/cad/:id', component: ClienteCadComponent },
  { path: 'cliente/list', component: ClienteListComponent },
  { path: 'product/cad', component: ProductCadComponent },
  { path: 'product/cad/:id', component: ProductCadComponent },
  { path: 'product/list', component: ProductComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
