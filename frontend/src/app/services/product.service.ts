import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Product} from '../interfaces/product.interface';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()
export class ProductService {
  constructor(private http: HttpClient) {
  }

  apiUrl = '/api/product';

  private handleError(error: HttpErrorResponse) {

    if (error.error.errors) {
      const mongoErrors = error.error.errors;
      let message = '';

      for (const key in mongoErrors) {
        if (mongoErrors.hasOwnProperty(key)) {
          message = `MongoDB error at field ${key}: ${mongoErrors[key].message}`;
          break;
        }
      }

      return throwError(message);
    } else if (error.error.message) {
      return throwError(error.error.message);
    } else if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  save(product: Product): Observable<Product> {
    return this.http.post<Product>(this.apiUrl, product)
      .pipe(
        catchError(this.handleError)
      );
  }

  uploadImage(product: Product): Observable<Product> {
    // @ts-ignore
    return this.http.post<Product>(this.apiUrl + '/uploadImage', product)
      .pipe(
        catchError(this.handleError)
      );
  }

  update(id: string, product: Product): Observable<Product> {
    return this.http.put<Product>(this.apiUrl + '/' + id, product)
      .pipe(
        catchError(this.handleError)
      );
  }

  getAll(): Observable<Product[]> {
    return this.http.get<Product[]>(this.apiUrl);
  }


  getById(id: string) {
    return this.http.get<Product>(this.apiUrl + '/' + id);
  }

  delete(id: string) {
    return this.http.delete(this.apiUrl + '/' + id);
  }
}
