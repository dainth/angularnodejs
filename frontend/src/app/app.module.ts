import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClienteCadComponent } from './components/cliente-cad.component';
import { ClienteListComponent } from './components/cliente-list.component';
import { HttpClientModule } from '@angular/common/http';
import { ClienteService } from './services/cliente.service';
import { ExportService } from './services/export.service';
import { ProductService} from './services/product.service';
import { ToastrModule } from 'ngx-toastr';
import { ProductComponent } from './components/product/product.component';
import { ProductCadComponent} from './components/product/product-cad.component';
import {defaultSimpleModalOptions, SimpleModalModule} from 'ngx-simple-modal';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    ClienteCadComponent,
    ClienteListComponent,
    ProductComponent,
    ProductCadComponent
  ],
    imports: [
        BrowserModule,
        NgxPaginationModule,
        AppRoutingModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        SimpleModalModule.forRoot({container: 'modal-container'}, {
            ...defaultSimpleModalOptions, ...{
                closeOnEscape: true,
                closeOnClickOutside: true,
                wrapperDefaultClasses: 'o-modal o-modal--fade',
                wrapperClass: 'o-modal--fade-in',
                animationDuration: 300,
                autoFocus: true
            }
        }),
        HttpClientModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right'
        }),
        FormsModule
    ],
  providers: [ ClienteService, ProductService, ExportService],
  bootstrap: [AppComponent]
})
export class AppModule { }
