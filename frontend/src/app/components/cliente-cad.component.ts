import {Component, ViewChild, AfterViewInit, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl, FormBuilder} from '@angular/forms';
import {ClienteService} from '../services/cliente.service';
import {Cliente} from '../interfaces/cliente.interface';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'cliente-cad',
  templateUrl: './cliente-cad.component.html',
})
export class ClienteCadComponent implements OnInit, AfterViewInit {

  id: string = null;
  routerSub: any;

  submited = false;
  message = null;
  error = false;

  form = this.initForm();

  constructor(
    private fb: FormBuilder,
    private clienteService: ClienteService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.routerSub = this.route.params.subscribe((params) => {
      this.id = params.id;
    });
  }

  initForm(client ?) {
    return this.fb.group({
      code: [client && client.code ? client.code : '', [Validators.required, Validators]],
      name: [client && client.name ? client.name : '', [Validators.required]],
      address: [client && client.address ? client.address : '', [Validators.required]],
      telephone: [client && client.telephone ? parseInt(client.telephone, 10) : '', [Validators.required]],
      status: [client && client.status ? parseInt(client.status, 10) : '', [Validators.required]],
      birthdate: [client && client.birthdate ? moment(client.birthdate).format('YYYY-MM-DD') : '', [Validators.required]],
    });
  }

  ngAfterViewInit() {

    if (this.id) {
      this.clienteService.getById(this.id)
        .subscribe(
          data => {
            this.form = this.initForm(data);
          },
          error => {
            this.message = error;
          }
        );
    }
  }

  onSubmit() {
    this.submited = true;

    if (this.form.valid) {
      const cliente: Cliente = this.form.value;

      if (this.id) {
        this.update(cliente);
      } else {
        this.create(cliente);
      }
    }
  }

  update(cliente
           :
           Cliente
  ) {
    this.clienteService.update(this.id, cliente)
      .subscribe(
        data => {
          this.submited = false;
          this.toastr.success('Sửa thành công', 'Sửa');
          this.router.navigate(['/cliente/list']);
        },
        error => {
          this.toastr.error('Sửa thất bại', 'Sửa');
        }
      );
  }

  create(cliente
           :
           Cliente
  ) {
    this.clienteService.save(cliente)
      .subscribe(
        data => {
          this.form.reset();
          this.submited = false;
          this.toastr.success('Thêm thành công', ' Thêm thành công');
          this.router.navigate(['/cliente/list']);
        },
        error => {
          this.toastr.error('Thêm thất bại', 'Thêm');
        }
      );
  }
}
