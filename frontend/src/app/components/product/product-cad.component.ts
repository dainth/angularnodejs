import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ProductService} from '../../services/product.service';
import {Product} from '../../interfaces/product.interface';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {ToastrService} from 'ngx-toastr';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'product-cad',
  templateUrl: './product-cad.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductCadComponent implements OnInit, AfterViewInit {

  id: string = null;
  routerSub: any;
  Number: number;
  submited = false;
  message = null;
  error = false;
  numberOld: number;
  checkNumberOld: boolean;

  form = this.initForm();

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.routerSub = this.route.params.subscribe((params) => {
      this.id = params.id;
    });
  }

  initForm(product ?) {
    return this.fb.group({
      code: [product && product.code ? product.code : '', [Validators.required]],
      name: [product && product.name ? product.name : '', [Validators.required]],
      date_manufacture: [product && product.date_manufacture ? moment(product.date_manufacture)
        .format('YYYY-MM-DD') : '', [Validators.required]],
      date_expiration: [product && product.date_expiration ? moment(product.date_expiration)
        .format('YYYY-MM-DD') : '', [Validators.required]],
      status: [product && product.status ? parseInt(product.status, 10) : '', [Validators.required]],
      number: [product && product.number ? parseInt(product.number, 10) : '', [Validators.required]],
    });

  }
  ngAfterViewInit() {
    if (this.id) {
      this.productService.getById(this.id)
        .subscribe(
          data => {
            this.form = this.initForm(data);
          },
          error => {
            this.message = error;
          }
        );
    }
  }
  onSubmit() {
    console.log(this.form.value, 222);
    // this.productService.uploadImage(picture)
    this.submited = true;
    if (this.form.valid) {
      const product: Product = this.form.value;
      if (this.id) {
        this.update(product);
      } else {
        this.create(product);
      }
    }
  }
  update(product: Product) {
    this.productService.update(this.id, product)
      .subscribe(
        data => {
          this.submited = false;
          this.toastr.success('Sửa thành công', 'Sửa');
          this.router.navigate(['/product/list']);
        },
        error => {
          this.toastr.error('Sửa thất bại', 'Sửa');
        }
      );
  }

  create(product: Product) {
    this.productService.save(product)
      .subscribe(
        data => {
          this.form.reset();
          this.submited = false;
          this.toastr.success('Thêm thành công', ' Thêm');
          this.router.navigate(['/product/list']);
        },
        error => {
          this.toastr.error('Thêm thất bại', 'Thêm');
        }
      );
  }

  changeOption(event) {
    if (!this.checkNumberOld) {
      this.numberOld = this.form.get('number').value;
      this.checkNumberOld = true;
    }
    if (parseInt(event.target.value, 10) === 2) {
      this.form.get('number').setValue(0);
    } else {
      this.form.get('number').setValue(this.numberOld);
    }
  }
}
