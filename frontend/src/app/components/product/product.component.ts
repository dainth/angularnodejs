import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Product} from '../../interfaces/product.interface';
import {ProductService} from '../../services/product.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  p: any;

  constructor(private productService: ProductService,
              private routeact: ActivatedRoute,
              private router: Router) {
    this.config = {
      currentPage: 1,
      itemsPerPage: 10,
      totalItems: 0
    };
    routeact.queryParams.subscribe(
      params => this.config.currentPage = params.page ? params.page : 1);
    for (let i = 1; i < 200; i++) {
      this.pageNumbers.push(i);
    }
  }

  config: any;
  name: string;
  pageNumbers: number[] = [];
  Number: number;
  fieldOrderBy: keyof Product = 'code';
  multiplierOrderBy: 1 | -1 = 1;
  products: any;

  productList() {
    this.productService.getAll().subscribe(
      (res: Product[]) => {
        console.log(res)
        this.products = res;
      }
    );

  }

  // loadList() {
  //   this.productService.getAll()
  //     .subscribe(data => {
  //       console.log(data)
  //       this.products = new Observable((ob) => {
  //           ob.next(data ? data.sort((a, b) =>
  //               a[this.fieldOrderBy] > b[this.fieldOrderBy] ? 1 * this.multiplierOrderBy : -1 * this.multiplierOrderBy)
  //             : []
  //           );
  //         });
  //       },
  //     );
  // }
  loadList() {
    this.productService.getAll()
      .subscribe(data => {
          this.products = data;
        },
      );
  }

  ngOnInit() {
    this.loadList();
  }

  // tslint:disable-next-line:variable-name
  separteAmount(Number) {
    return Intl.NumberFormat('de-DE').format(Number);
  }

  pageChange(newPage: number) {
    this.router.navigate(['/product/list'], {queryParams: {page: newPage}});
  }

  onRemove(id) {
    console.log(id);
    if (confirm('Bạn có muốn xóa sản phẩm này không?')) {
      this.productService.delete(id).subscribe(
        data => {
          this.loadList();
        },
        error => {
          console.error(error);
        }
      );
    }
  }

  Search() {
    if (this.name !== '') {
      this.products = this.products.filter(res => {
        return res.name.toLocaleLowerCase().match(this.name.toLocaleLowerCase());
      });
    } else if (this.name === '') {
      this.ngOnInit();
    }
  }

  // export() {
  //
  //   const jsonArray: Cliente[] = [];
  //
  //   this.productList.forEach(element => {
  //     // @ts-ignore
  //     element.forEach(c => jsonArray.push(c));
  //   });

  // this.exportService.exportExcel(jsonArray, 'clients');
  // }

  // orderBy(field: keyof Cliente) {
  //
  //   if (field !== this.fieldOrderBy) {
  //     this.fieldOrderBy = field;
  //     this.multiplierOrderBy = 1;
  //   } else {
  //     this.multiplierOrderBy *= -1;
  //   }
  //
  //   this.fieldOrderBy = field;
  //   this.loadList();
  // }
}
